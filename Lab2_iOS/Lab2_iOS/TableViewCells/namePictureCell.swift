//
//  namePictureCell.swift
//  Lab2_iOS
//
//  Created by Julijus Ergul on 2018-01-15.
//  Copyright © 2018 Julijus Ergul. All rights reserved.
//

import UIKit

class namePictureCell: UITableViewCell {

    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
