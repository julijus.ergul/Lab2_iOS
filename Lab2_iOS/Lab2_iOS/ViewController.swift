//
//  ViewController.swift
//  Lab2_iOS
//
//  Created by Julijus Ergul on 2017-12-27.
//  Copyright © 2017 Julijus Ergul. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        switch section{
        case 0:
            return 5
        case 1:
            return 3
        case 2:
            return 1
        default:
            return 0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.section == 0 && indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "nameCell", for: indexPath) as! namePictureCell
            cell.firstName.text = "Julijos"
            cell.lastName.text = "Ergül"
            cell.profileImage.image = UIImage(named: "julijus")
            return cell
        }
        else if indexPath.section == 0 && indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "myPhoneNumCell", for: indexPath) as! myPhoneNumCell
            cell.phoneNumber.text  = "0736376063"
            return cell
        }
        else if indexPath.section == 0 && indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "myAddressCell", for: indexPath) as! myAddressCell
            cell.adress.text = "Björngränd 8"
            return cell
        }
        else if indexPath.section == 0 && indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "myEmailCell", for: indexPath) as! myEmailCell
            cell.emailLabel.text = "erju1293@ju.se"
            return cell
        }
        else if indexPath.section == 0 && indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "favouriteWebSiteCell", for: indexPath) as! favouriteWebSiteCell
            cell.webSite.text = "youtube.com"
            return cell
        }
        else if indexPath.section == 1 && indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "workIndustrikompetensCell", for: indexPath) as! workIndustrikompetensCell
            cell.workIndustriKomp.text = "Industrikompetens"
            return cell
        }

        else if indexPath.section == 1 && indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "schoolEbersteinskaGymCell", for: indexPath) as! schoolEbersteinskaGymCell
            cell.schoolEber.text = "Ebersteinska Gymnasiet"
            return cell
        }
        else if indexPath.section == 1 && indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "schoolJonkonpingsUniCell", for: indexPath) as! schoolJonkonpingUniCell
            cell.schoolJU.text = "Jönköpings University"
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "myPhoneNumCell", for: indexPath) as! myPhoneNumCell
            cell.phoneNumber.text = "Demo One"
            return cell
        }
        
    }
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1{
            return "Work Experience"
        }
        if section == 2{
            return "Demos"
        }
        else{
            return ""
        }
    }
    
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    
        if indexPath.section == 0 && indexPath.row == 1{
            UIApplication.shared.open(URL(string: "tel//\("0736376063")")!)
        }
        
        if indexPath.section == 0 && indexPath.row == 2{
            performSegue(withIdentifier: "mainToAdress", sender: indexPath)
        }
        
        if indexPath.section == 0 && indexPath.row == 3{
            UIApplication.shared.open(URL(string: "mailto: erju1293@ju.se")!)
        }
        
        if indexPath.section == 0 && indexPath.row == 4{
            performSegue(withIdentifier: "mainToWeb", sender: indexPath)
        }
        
        if indexPath.section == 1{
            performSegue(withIdentifier: "mainToWork", sender: indexPath)
        }
        
        if indexPath.section == 2{
            performSegue(withIdentifier: "mainToAnim", sender: indexPath)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let workController = segue.destination as? WorkExperienceViewController, let indexPath = sender as? IndexPath{
            if indexPath.section == 1 && indexPath.row == 0 {
                workController.dataImage = UIImage(named: "idukomp")
                workController.data = "Ett bemaningsföretag. Arbetade som lagerarbetare."
                workController.lbl = "Industikompetens"
            }
            if indexPath.section == 1 && indexPath.row == 1 {
                workController.dataImage = UIImage(named: "ju")
                workController.data = "Skolan jag läser på för tillfället."
                workController.lbl = "Jönköpings Universitet"
            }
            if indexPath.section == 1 && indexPath.row == 2 {
                workController.dataImage = UIImage(named: "eber")
                workController.data = "Gymnasieskolan jag läste på."
                workController.lbl = "Ebersteinska Gymnasiet"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

