//
//  AddressViewController.swift
//  Lab2_iOS
//
//  Created by Julijus Ergul on 2018-01-15.
//  Copyright © 2018 Julijus Ergul. All rights reserved.
//

import UIKit
import MapKit

class AddressViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let startLocation = CLLocation(latitude: 57.780974, longitude: 14.181022)
        let regionRadius: CLLocationDistance = 1000
        
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(startLocation.coordinate, regionRadius, regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: 57.780974, longitude: 14.181022)
        mapView.addAnnotation(annotation)
        
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
