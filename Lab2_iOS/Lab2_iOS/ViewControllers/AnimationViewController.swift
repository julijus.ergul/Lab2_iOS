//
//  AnimationViewController.swift
//  Lab2_iOS
//
//  Created by Julijus Ergul on 2018-01-15.
//  Copyright © 2018 Julijus Ergul. All rights reserved.
//

import UIKit

class AnimationViewController: UIViewController {


    @IBOutlet weak var centerXConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerYConstraint: NSLayoutConstraint!
    @IBOutlet weak var animLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        animLabel.center.x -= view.bounds.width
        UIView.animate(withDuration: 1.0, delay: 0.2, options: [.curveEaseIn], animations:{
            self.animLabel.center.x += self.view.bounds.width
            self.view.layoutIfNeeded()
        } )
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
