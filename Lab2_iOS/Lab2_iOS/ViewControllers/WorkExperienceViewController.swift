//
//  WorkExperienceViewController.swift
//  Lab2_iOS
//
//  Created by Julijus Ergul on 2018-01-15.
//  Copyright © 2018 Julijus Ergul. All rights reserved.
//

import UIKit

class WorkExperienceViewController: UIViewController {

    @IBOutlet weak var workLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var txtView: UITextView!
    
    var dataImage: UIImage? = nil
    var data  = ""
    var lbl = "SomeWork"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if dataImage != nil {
            imgView.image = dataImage
        }
        txtView.text = data
        workLabel.text = lbl
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
