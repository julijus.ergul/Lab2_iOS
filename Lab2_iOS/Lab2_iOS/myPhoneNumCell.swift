//
//  viewControllerTableViewCellOne.swift
//  Lab2_iOS
//
//  Created by Julijus Ergul on 2017-12-28.
//  Copyright © 2017 Julijus Ergul. All rights reserved.
//

import UIKit

class myPhoneNumCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
